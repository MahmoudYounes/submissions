#include<iostream>
#include<vector>
#include<queue>
using namespace std;
vector<vector<int>> AdjList;
vector<int> color;
bool BiBfs(int s)
{
	bool NBiC = 0;
	queue<int> q;
	q.push(s);
	color[s] = 0;
	while (!q.empty())
	{
		int u = q.front();
		q.pop();
		for (int i = 0; i < AdjList[u].size(); i++)
		{
			int v = AdjList[u][i];
			if (color[v] == -1)
			{
				color[v] = 1 - color[u];
				q.push(v);
			}
			else if (color[v] == color[u])
			{
				NBiC = 1;
				break;
			}
		}		
		if (NBiC == 1) break;
	}
	return NBiC;
}

int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int n,m;
	while (cin >> n)
	{
		if (n == 0) break;
		AdjList.resize(n);
		color.assign(n, -1);
		cin >> m;
		int x, y;
		for (int i = 0; i < m; i++)
		{
			cin >> x >> y;
			AdjList[x].push_back(y);
			AdjList[y].push_back(x);
		}
		bool cond = BiBfs(0);
		if (cond)
			cout << "NOT BICOLORABLE." << endl;			
		else cout << "BICOLORABLE." << endl;
		AdjList.clear();
		color.clear();				
	}
}