#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <string>
#include <string.h>
#include <limits.h>
using namespace std;
int main()
{
	freopen("input.txt","r",stdin);
	freopen("output.txt","w",stdout);
	
	int n,x;	
	scanf("%d",&n);	
	vector<vector<int> > vec (5010,vector<int>(2,0));
	vector<pair<int,int> > ans(n+2);
	int ansidx=0;
	for (int i=0;i<n*2;i++)			//O(6*10^5) 
	{
		scanf("%d",&x);			
		if (vec[x][0]==0) vec[x][0]=i+1;
		else if (vec[x][1]==0) vec[x][1]=i+1;
		if(vec[x][0]!=0&&vec[x][1]!=0)
		{
			ans[ansidx]=make_pair(vec[x][0],vec[x][1]);
			ansidx++;
			vec[x][0]=0;
			vec[x][1]=0;
		}		
	}	
	if(ansidx<n) cout<<-1<<endl;
	else
	{
		for (int i=0;i<ansidx;i++) //O(3*10^5)
		{
			printf("%d %d\n",ans[i].first,ans[i].second);
		}
	}
	//all O(6*10^5+3*10^5=9*10^5)

}