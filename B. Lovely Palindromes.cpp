#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <string>

using namespace std;

#define DRI(x) int (x); scanf("%d", &(x))
#define RI(x) scanf("%d", &(x))
#define REP(i,n) for (int (i) = 0;(i) < (n);(i)++)
#define pii(p) pair<int, int> (p)
#define vpii(p) vector<pair<int, int> > (p);
#define vpiin(p,n) vector<pair<int, int> > (p)(n);
#define vi(p, n)vector<int> (p);
#define vin(p, n)vector<int> (p)(n);


int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	string s;
	cin >> s;
	cout << s;
	reverse(s.begin(), s.end());
	cout << s << endl;

	return 0;
}