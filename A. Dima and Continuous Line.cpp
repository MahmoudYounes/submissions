#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <fstream>
#include <map>
using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE	
		freopen("in.txt","r",stdin);
	#endif
    
    int n;
    cin>>n;
    if (n==1) {cout<<"no"<<endl;return 0;}
    vector<pair<int,int> > arr(n-1);
    cin>>arr[0].first;
    for (int i=0;i<n-1;i++)
    {
    	cin>>arr[i].second;
    	if (i+1<n-1)
    		arr[i+1].first=arr[i].second;
    	if (arr[i].first>arr[i].second) swap(arr[i].first,arr[i].second);
    	//cout<<"at i="<<i<<" input is "<<arr[i].first<<' '<<arr[i].second<<endl;
    }
    bool cond=1;
    for (int i=0;i<n-1;i++)
    {
    	for (int j=0;j<n-1;j++)
    	{       		
			if ( (arr[i].first<arr[j].first && arr[j].first<arr[i].second && arr[i].second<arr[j].second) || 
				 (arr[j].first<arr[i].first && arr[i].first<arr[j].second && arr[j].second<arr[i].second))
			{
				cond=0;
				break;
			}
			//cout<<arr[i].first<<' '<<arr[i].second<<' '<<arr[j].first<<' '<<arr[j].second<<' '<<cond<<endl;
	
    	}
    	if (!cond)
    	{
    		break;
    	}
    }
    if (cond) cout<<"no"<<endl;
    else cout<<"yes"<<endl;
    return 0;
}