#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <limits.h>
using namespace std;

int n,w,h,mi;
vector<pair<int,int> >vec;
vector<int> temp;

int sol(int idx,int lidx)
{	
	if (idx>=n)
	{
		return 0;			
	}
	if (mi!=idx&&vec[idx].first>vec[lidx].first&&vec[idx].second>vec[lidx].second)
	{
		temp.push_back(idx);
		return 1+sol(idx+1,idx);
	}
	else return sol(idx+1,idx);	

}

int main()
{
	#ifndef ONLINE_JUDGE
	freopen("in.txt","r",stdin);
	#endif
	
	cin>>n>>w>>h;	
	vec.resize(n);
	for (int i=0;i<n;i++)
	{
		scanf("%d %d",&vec[i].first,&vec[i].second);
	}
	int max_no=INT_MIN;
	vector<int> *path;
	for (int i=0;i<n;i++)
	{
		if(vec[i].first>w&&vec[i].second>h)
		{
			mi=i;
			int mrtempo=sol(0,i);
			if(max_no<mrtempo)
			{
				max_no=mrtempo;
				path->clear();
				path=new vector<int>(temp);				
			}			
		}
	}
	cout<<max_no<<endl;
	for (int i=0;i<(*path).size();i++)
		cout<<(*path)[i]<<' ';
	cout<<endl;

}