#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>

using namespace std;

#define RI(x) scanf("%d", &(x))
#define DRI(x) int (x); RI((x))
#define DII(x, y) RI((x)); RI((y))
#define DRII(x, y) DRI((x)); DRI((y))

#define vi(vec) vector<int> (vec)
#define vpii(vec) vector<pair<int, int> > (vec)

#define lop(i, s, e) for(int (i) = (s);(i) < (e);(i)++)
#define lopi(i, s, e, inc) for(int (i) = (s);(i) < (e);(i) += (inc))


int moda[10000000];
int modb[10000000];


int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
		#define DEBUG 1
	#endif

	string s;
	
	cin >> s;
	DRII(a, b);

	int n = s.length(), xn = s[0] - '0', yn = s.back() - '0', x, y, mul = 1;
	moda[0] = xn % a;
	modb[s.length() - 1] = yn % b;
	for (int i = 1;i < n;i++)
	{
		x = s[i] - '0';

		mul *= 10;
		mul %= b;
		y = ((s[n - i - 1] - '0') * mul) % b;
		
		xn = ((xn * 10) % a + x % a) % a;
		moda[i] = xn;

		yn = ((yn) % b + y % b) % b;
		modb[n - i - 1] = yn;
	}

	int idx = 0, cond = 0;
	for (;idx < n - 1;idx++)
	{
		if (moda[idx] == 0 && modb[idx + 1] == 0 && s[idx + 1] != '0')
		{
			cond = 1;
			break;	
		}	
	}

	if (cond)
	{
		cout << "YES\n";
		cout << s.substr(0, idx + 1) << endl;
		cout << s.substr(idx + 1) << endl;
	}
	else
		cout << "NO\n";
	return 0;
}