#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF 1<<30
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

class unionF
{
private:
	vi p;
	vi r;
	vi c;	
public:
	unionF()
	{}
	void insert(int i)
	{
		if (i >= p.size())
		{
			p.push_back(i);
			r.push_back(0);
			c.push_back(1);
		}
	}

	bool isSameSet(int a, int b)
	{
		return parent(a) == parent(b);
	}
	int parent(int a)
	{
		if (p[a] == a)return a;
		return p[a] = parent(p[a]);
	}
	void join(int a, int b)
	{
		if (!isSameSet(a, b))
		{
			int p1 = parent(a), p2 = parent(b);
			int r1 = r[p1], r2 = r[p2];
			if (r1 > r2)
			{
				p[p2] = p1;
				c[p1]+=c[p2];
			}
			else
			{
				p[p1] = p2;
				c[p2]+=c[p1];
				if (r1==r2)
					r[p2]++;
			}			
		}		
	}
	int countSet(int a)
	{
		int x = parent(a);
		return c[x];
	}

};

int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#ifdef DEBUG_MODE
	freopen("out.txt", "w", stdout);
#endif
#endif	
#ifdef DEBUG_MODE
	ifstream in;
	in.open("o2c.txt", ios::in);
	ofstream out;
	out.open("ost.txt", ios::out);
	int line_no = 0, global_count = 0;;
#endif
	DRI(t);
	while (t--)
	{
		map<string, int>cypher;
		unionF uf;
		DRI(n);
		int k = 0;				
		for (int i = 0; i < n; i++)
		{
			string s1, s2;
			cin >> s1 >> s2;
			
			if (cypher.find(s1) == cypher.end())
			{
				cypher[s1] = k;
				uf.insert(k++);
			}
			if (cypher.find(s2) == cypher.end())
			{
				cypher[s2] = k;
				uf.insert(k++);
			}
			uf.join(cypher[s1], cypher[s2]);
			int no = uf.countSet(cypher[s1]);
			cout << no <<endl;

#ifdef DEBUG_MODE
			int in_no;
			in >> in_no;
			if (in_no != no) out << line_no++ << ' ' << global_count++ << endl;
			else line_no++;
#endif
		}		
				
	}

	return 0;
}