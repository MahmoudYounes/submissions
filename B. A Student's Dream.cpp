#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <string>
#include <string.h>
#include <limits.h>
using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE
	freopen("in.txt","r",stdin);
	#endif
	int lg,rg,lb,rb;
	cin>>lg>>rg>>lb>>rb;
	if (rb>=lg-1&&rb<=(lg+1)*2)
	{
		cout<<"YES"<<endl;		
	}
	else if (lb>=rg-1&&lb<=(rg+1)*2)
	{
		cout<<"YES"<<endl;
	}
	else cout<<"NO"<<endl;
}