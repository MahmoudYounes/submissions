#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	long long arr[3], t = 0;

	cin >> arr[0] >> arr[1] >> arr[2];
	sort(arr, arr + 3);
	if (arr[2] > 2 * (arr[0] + arr[1]))
		arr[2] = 2 * (arr[0] + arr[1]);
	cout << (arr[0] + arr[1] + arr[2]) / 3 << endl;
}