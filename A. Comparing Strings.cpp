#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt","r",stdin);
	#endif

	
	string p,s;
	
	cin>>p>>s;
	
	if (p.length()!=s.length())
	{ 
		cout<<"NO"<<endl;
		return 0;
	}

	bool cond=0;
	int n=p.length(),m=s.length(),mh=0;
	vector<int> mhind;
	for (int i=0;i<n;i++)
	{
		if (i<m)
		{
			if (p[i]!=s[i])
			{
				mh++;
				mhind.push_back(i);
			}
		}
		else break;
	}
	if (mh<=2&&p[mhind[0]]==s[mhind[1]]&&p[mhind[1]]==s[mhind[0]]) cond=1;
	if (cond) cout<<"YES"<<endl;
	else cout<<"NO"<<endl;
	return 0;

}