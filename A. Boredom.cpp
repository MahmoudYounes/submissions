#include <iostream>
#include <fstream>
#include <vector>
#include <limits.h>
#include <string.h>
#include <algorithm>

using namespace std;

#define A_SIZE 100100

int n;
long long arr[A_SIZE];
long long dp[A_SIZE];


long long rec(int idx)
{	
	if(idx >= A_SIZE) return (long long) 0;
	if(dp[idx] != -1) return (long long) dp[idx];
	long long rec1 = (long long) idx * arr[idx] + rec(idx + 2);
	return dp[idx] = (long long) max(rec1, (long long)rec(idx + 1));
}

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif	

	memset(dp, -1, sizeof (dp));
	int x;
	cin >> n;
	for (int i = 0;i < n;i++)
	{
		cin >> x;
		arr[x]++;
	}
	cout << rec(1) << endl;
	return 0;
}