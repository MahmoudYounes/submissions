#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <limits.h>

using namespace std;

#define DRI(x) int (x); scanf("%d", &(x))
#define RI(x) scanf("%d", &(x))
#define REP(i,n) for (int (i) = 0;(i) < (n);(i)++)
#define pii(p) pair<int, int> (p)
#define vpii(p) vector<pair<int, int> > (p);
#define vpiin(p,n) vector<pair<int, int> > (p)(n);
#define vi(p, n)vector<int> (p);
#define vin(p, n)vector<int> (p)(n);


int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	DRI(n);
	vpiin(vec,n);
	int a,b;
	int maxN = INT_MIN;
	REP(i,n)
	{
		RI(a);
		RI(b);
		vec[i].first  = a;
		vec[i].second = b;
	}
	if (n == 1)
	{
		cout << min(vec[0].first, vec[0].second) << endl;
		return 0;
	}
	sort(vec.begin(),vec.end());
	int finalD = min(vec[0].first,vec[0].second), minN = INT_MAX;
	REP(i,n)
	{
		minN = min(vec[i].first, vec[i].second);
		if (finalD <= minN)
			finalD = minN;
		else
			finalD = vec[i].first;
	}
	cout << finalD << endl;
	return 0;
}