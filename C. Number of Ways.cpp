#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int main()
{
	long long n;
	long long suma[500000], sumc = 0;
	long long arr[500000];

	cin >> n;
	for (int i = 0;i < n;i++)
	{
		cin >> arr[i];
		sumc += arr[i];
	}
	if(sumc % 3 != 0)
	{
		cout << 0 << endl;
		return 0;
	}   	   		
	long long sumf = sumc / 3, cum = 0;
	long long ways = 0;
	for (int i = n-1; 1 <= i;i--)
	{
		if (i == n-1)
		{
			cum = arr[n-1];
			suma[i] = (arr[n-1] == sumf);
			continue;
		}
		else cum += arr[i];
		suma[i] = suma[i+1] + (cum == sumf);
	}
	sumc = 0;
	for (int i = 0;i < n - 2;i++)
	{
		sumc += arr[i];
		if (sumc == sumf)
			ways += suma[i+2];
	}
	cout << ways << endl;
	return 0;
}