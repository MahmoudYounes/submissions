#include <iostream>
#include <string.h>
#include <math.h>
#include <cmath>
using namespace std;
int main()
{
    int arr[1010],x,n;
    memset(arr,0,sizeof(arr));
    cin>>n;
    for (int i=0;i<n;i++)
    {
        cin>>x;
        arr[x]++;
    }
    bool can=true;            
    int mx=ceil(float(0.5*n));
    for (int i=0;i<=1000;i++)
    {        
        if (arr[i]>mx)
        {
            can=false;         
        }      
    }    
    
    if(can) cout<<"YES"<<endl;
    else cout<<"NO"<<endl;
}