#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF 1<<30
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

class unionF
{
private:
	vi p;
	vi r;
	vi c;
	int awake;
	int un_awake;
	bool act[3];
public:
	bool flag;
public:
	unionF(int n)
	{
		p.assign(n, 0);
		r.assign(n, 0);
		c.assign(n, 1);
		fl2(i, n)p[i] = i;
		awake = 0;
		un_awake = 0;
		memset(act, 0, sizeof(act));
	}

	bool isSameSet(int a, int b)
	{
		return parent(a) == parent(b);
	}
	int parent(int a)
	{
		if (p[a] == a)return a;
		return p[a] = parent(p[a]);
	}
	void join(int a, int b)
	{
		if (!isSameSet(a, b))
		{		
		}
	}
	void checkAwake()
	{
		if (parent(0) == parent(1) && parent(1) == parent(2) && parent(0) == parent(2))
			flag = true;
	}
	int countSet(int a)
	{
		int x = parent(a);
		return c[x];
	}
	int sol()
	{
		return awake;
	}
};

int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#ifdef DEBUG_MODE
	freopen("out.txt", "w", stdout);
#endif
#endif	
#ifdef DEBUG_MODE
	ifstream in;
	in.open("o2c.txt", ios::in);
	ofstream out;
	out.open("ost.txt", ios::out);
	int line_no = 0, global_count = 0;;
#endif
	int ac;
	while (cin >> ac)
	{
		map<char, int>cypher;
		unionF uf(ac);
		DRI(n);
		string x;
		getline(cin, x);
		getline(cin, x);
		int k = 0;
		fl2(i, x.size())
		{
			if (isalpha(x[i]))
			{
				cypher[x[i]] = k++;
			}
		}
		char a, b;
		for (int i = 0; i < n; i++)
		{
			cin >> a >> b;
			if (cypher.find(a) == cypher.end())
			{
				cypher[a] = k++;
			}
			if (cypher.find(b) == cypher.end())
			{
				cypher[b] = k++;
			}
			uf.join(cypher[a], cypher[b]);

#ifdef DEBUG_MODE
			int in_no;
			in >> in_no;
			if (in_no != no) out << line_no++ << ' ' << global_count++ << endl;
			else line_no++;
#endif
		}
		if (!uf.flag)
			cout << "THIS BRAIN NEVER WAKES UP" << endl;
		else cout << "WAKE UP IN, " << uf.sol() + 1 << ", YEARS" << endl;
	}
	return 0;
}