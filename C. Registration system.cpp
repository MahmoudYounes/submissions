#include <iostream>
#include <string>
#include <map>
#include <fstream>
#include <stdio.h>
using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE
	freopen("in.txt","r",stdin);
	#endif

	int n;
	scanf("%d",&n);
	map <string,int> names;
	char s[1020];
	for (int i=0;i<n;i++)
	{
		scanf("%s",&s);
		if(names.find(s)!=names.end())
		{			
			printf("%s%d\n",s,++names[s]);
		}
		else
		{
			names[s]=0;
			printf("OK\n");
		}
	}
	return 0;

}