#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#define RI(x) scanf("%d", &(x))
#define DRI(x) int (x); RI((x))
#define DII(x, y) RI((x)); RI((y))
#define DRII(x, y) DRI((x)); DRI((y))

#define vi(vec) vector<int> (vec)
#define vpii(vec) vector<pair<int, int> > (vec)

#define lop(i, s, e) for(int (i) = (s);(i) < (e);(i)++)
#define lopi(i, s, e, inc) for(int (i) = (s);(i) < (e);(i) += (inc))

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	DRII(n, m);
	int s = min(n, m) + 1;
	vpii(vec);
	lop(i, 0, s)
	{
		vec.push_back(make_pair(i, s - i - 1));
	}
	printf("%d\n", vec.size());
	lop(i, 0, vec.size())
	{
		printf("%d %d\n", vec[i].first, vec[i].second);
	}
	return 0;
}