#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF INT_MAX
#define NINF INT_MIN
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

int arrx[105];
int arry[105];
int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	cout << fixed << setprecision(12);
	double a1, b1, a2, b2;
	double c1, d1, c2, d2;
	while (cin >> a1)
	{
		cin >> b1 >> a2 >> b2 >> c1 >> d1 >> c2 >> d2;
		double ss1 = (b1 - a1 + 1)*(b2 - a2 + 1);
		double ss2 = (d1 - c1 + 1)*(d2 - c2 + 1);
		double xly = 0, xmy = 0, xey = 0;
		memset(arrx, 0, sizeof(arrx));
		memset(arry, 0, sizeof(arry));
		for (int i = a1; i <= b1;i++)
		for (int j = a2; j <= b2; j++)		
			arrx[i + j]++;
		for (int i = c1; i <= d1; i++)
		for (int j = c2; j <= d2; j++)
			arry[i + j]++;
		//p(x<y)
		for (int i = a1 + a2; i <= b1 + b2;i++) //loop through X
		for (int j = c1 + c2; j <= d1 + d2; j++) //loop through Y
		{
			if (i < j)			
				xly++;
			else if (i>j)xmy++;
			else xey++;			
		}
		if (xly > xmy&&xly>xey)cout << "Emma" << endl;
		else if (xly<xmy&&xmy>xey)cout << "Gunnar" << endl;
		else if(xey>xly&&xey>xmy)cout << "Tie"<<endl;
		else cout << "Tie" << endl;
	}
	return 0;
}