#include <iostream>
#include <vector>
#include <string>
#include <string.h>
#include <map>
#include <algorithm>
#include <limits.h>
#include <stdio.h>
#include <cstdio>
#include <set>
#include <queue>
#include <fstream>
#define INF 1<<30
using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE
	freopen("in.txt","r",stdin);
	#endif

	int n,ans=0;
	cin>>n;
	for (int i=0;i<30;i++)
	{		
		
		ans+=(n&(1<<i))>0?1:0;
	}
	cout<<endl<<ans<<endl;
}