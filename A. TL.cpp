#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
	int n,m;
	cin>>n>>m;
	vector<int> vecr(n);
	vector<int> vecw(m);
	int maxr=INT_MIN,minr=INT_MAX,minw=INT_MAX;
	for (int i=0;i<n;i++)
	{
		cin>>vecr[i];
		maxr=max(maxr,vecr[i]);
		minr=min(minr,vecr[i]);
	}
	for (int i=0;i<m;i++)
	{
		cin>>vecw[i];
		minw=min(vecw[i],minw);
	}
	if (minw<=maxr||minw<=2*minr) cout<<-1<<endl;
	else cout<<max(2*minr,maxr)<<endl;

}