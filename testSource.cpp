#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF INT_MAX
#define NINF INT_MIN
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

#define UNVISITED 0
#define EXPLORED 1
#define VISITED 2

vi dfs_low, dfs_num, dfs_parent, articulation_vertex;
vector<vii> AdjList;
int dfsNumberCounter, dfsRoot, rootChildren;

void articulationPointAndBridge(int u) {
	dfs_low[u] = dfs_num[u] = dfsNumberCounter++; // dfs_low[u] <= dfs_num[u]
	for (int j = 0; j < (int)AdjList[u].size(); j++) {
		ii v = AdjList[u][j];
		if (dfs_num[v.first] == UNVISITED) { // a tree edge
			dfs_parent[v.first] = u;
			if (u == dfsRoot) rootChildren++; // special case if u is a root
			articulationPointAndBridge(v.first);
			if (dfs_low[v.first] >= dfs_num[u]) // for articulation point
				articulation_vertex[u] = true; // store this information first
			if (dfs_low[v.first] > dfs_num[u]) // for bridge
				printf(" Edge (%d, %d) is a bridge\n", u, v.first);
			dfs_low[u] = min(dfs_low[u], dfs_low[v.first]); // update dfs_low[u]
		}
		else if (v.first != dfs_parent[u]) // a back edge and not direct cycle
			dfs_low[u] = min(dfs_low[u], dfs_num[v.first]); // update dfs_low[u]
	}
}
int main()
{
	freopen("in.txt", "r", stdin);
	int V, E, x, y;
	while (cin >> V >> E)
	{
		AdjList.resize(V + 1);
		for (int i = 0; i < E; i++)
		{
			cin >> x >> y;
			AdjList[x].push_back(make_pair(y, 1));
			AdjList[y].push_back(make_pair(x, 1));
		}
		dfsNumberCounter = 0; dfs_num.assign(V + 1, UNVISITED); dfs_low.assign(V + 1, 0);
		dfs_parent.assign(V + 1, 0); articulation_vertex.assign(V + 1, 0);
		printf("Bridges:\n");
		for (int i = 0; i <= V; i++)
		if (dfs_num[i] == UNVISITED) {
			dfsRoot = i; rootChildren = 0; articulationPointAndBridge(i);
			articulation_vertex[dfsRoot] = (rootChildren > 1);
		} // special case		
		printf("Articulation Points:\n");
		for (int i = 0; i <= V; i++)
		if (articulation_vertex[i])
			printf(" Vertex %d\n", i);

		cout << endl;

		AdjList.clear();
		dfs_low.clear();
		dfs_num.clear();
		dfs_parent.clear();
		articulation_vertex.clear();		
	}
}