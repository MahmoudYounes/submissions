#include <iostream>
#include <fstream>
#include <map>
#include <vector>
using namespace std;

int main()
{
	#ifndef ONLINE_JUDGE
	 freopen("in.txt", "r", stdin);
	#endif

	int arr[150],n;
	vector<pair<int,int>> ans;
	cin >> n;
	for (int i = 0;i < n;i++)
	{
		cin >> arr[i];
	}
	int i = 0, prev = 0, sum = arr[0];
	while(i < n)
	{
		if(sum != 0)
		{
			ans.push_back(make_pair(prev, i));
			prev = i + 1;
			sum = 0;
		}
		else
		{
			sum += arr[i];
		}
		i++;
	}
	if (ans.size()!=0)
	{
		cout << "YES\n";
		cout << ans.size() << endl;
		for (int i = 0;i < ans.size();i++)
		{
			cout << ans[i].first + 1 << ' ' << ans[i].second + 1 << endl;
		}
	}
	else
		cout << "NO\n";
}