#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>

using namespace std;

#define RI(x) scanf("%d", &(x))
#define DRI(x) int (x); RI((x))
#define DII(x, y) RI((x)); RI((y))
#define DRII(x, y) DRI((x)); DRI((y))

#define vi(vec) vector<int> (vec)
#define vpii(vec) vector<pair<int, int> > (vec)

#define lop(i, s, e) for(int (i) = (s);(i) < (e);(i)++)
#define lopi(i, s, e, inc) for(int (i) = (s);(i) < (e);(i) += (inc))

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	DRI(n);
	vector<vector<int> > vec(n, vector<int>(n));

	vector<long long> add(n * 2);
	vector<long long> diff(n * 2);

	
	for (int i = 0;i < n;i++)
	{
		for (int j = 0;j < n;j++)
		{
			scanf("%d", &vec[i][j]);
		}
	}

	int x1, y1, x2, y2;
	long long maxO = INT_MIN, maxE = INT_MIN;
	for (int i = 0;i < n;i++)
	{
		for (int j = 0;j < n;j++)
		{
			add[i + j] += vec[i][j];
			diff[n - i + j - 1] += vec[i][j];
		}
	}
	
	for (int i = 0;i < n;i++)
	{
		for (int j = 0;j < n;j++)
		{
			if ((i + j) % 2)
			{
				if (maxO < (add[i + j] + diff[n - i + j - 1] - vec[i][j]))
				{
					maxO = (add[i + j] + diff[n - i + j - 1] - vec[i][j]);
					x1 = i;
					y1 = j;
				}
			}
			else
			{
				if (maxE < (add[i + j] + diff[n - i + j - 1] - vec[i][j]))
				{
					maxE = (add[i + j] + diff[n - i + j - 1] - vec[i][j]);
					x2 = i;
					y2 = j;
				}	
			}

		}
	}
	cout << maxO + maxE << endl;
	cout << x1 + 1 << ' ' << y1 + 1 << ' ' << x2 + 1 << ' ' << y2 + 1  << endl;
	return 0;
}