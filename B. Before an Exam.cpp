#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <cstdlib>
#include <string.h>
#include <list>
using namespace std;

vector<pair<int,int> > vec;
vector <int> ans;
int n,s;
int dp[35][250];

bool sol(int idx,int sum)
{		
	if(sum<0) return 0;
	if(idx>=n)
	{
		if(sum!=0)
		{			
			return 0;
		}
		else 
		{
			printf("YES\n");
			for (int i=0;i<ans.size();i++)
			{
				printf("%d ",ans[i]);
			}
			printf("\n");
			exit(0);
			
		}
	}
	if(dp[idx][sum]!=-1)return dp[idx][sum];
	bool cond=0;
	for (int i=vec[idx].second;i>=vec[idx].first;i--)
	{			
		ans.push_back(i);
		cond|=sol(idx+1,sum-i);
		if(cond)		
		{
			break;
		}
		ans.erase(ans.end()-1);		
	}
	return dp[idx][sum]=cond;
}

int main()
{
	#ifndef ONLINE_JUDGE
	freopen("in.txt","r",stdin);
	#endif

	memset(dp,-1,sizeof(dp));
	cin>>n>>s;
	vec.resize(n);
	for (int i=0;i<n;i++)
	{
		scanf("%d %d",&vec[i].first,&vec[i].second);
	}
	sol(0,s);
	printf("NO\n");
	return 0;

}