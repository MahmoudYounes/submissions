#include <iostream>
#include <map>
#include <string>
#include <algorithm>
#include <limits.h>
using namespace std;
int main()
{
	int n,max_n=INT_MIN;
	cin>>n;
	string s,o;
	map <string,int> mp;
	for (int i=0;i<n;i++)
	{
		cin>>s;
		if(mp.find(s)==mp.end())
		{
			mp[s]=1;
		}
		else mp[s]++;		
		if(mp[s]>max_n)
		{
			max_n=mp[s];
			o=s;
		}
	}
	cout<<o<<endl;

}