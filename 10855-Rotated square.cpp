#include <iostream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
using namespace std;





int main(){

	int x, y;
	while(scanf(" %d %d\n", &x, &y) && x != 0 && y != 0){		
		vector<vector<char> > N(x, vector<char>(x));
		vector<vector<char> > n(y, vector<char>(y));
		vector<vector<char> > n90(y, vector<char>(y));
		vector<vector<char> > n180(y, vector<char>(y));
		vector<vector<char> > n270(y, vector<char>(y));
		
		char tmp, b;
		for (int i = 0;i < x;i++){
			for (int j = 0;j < x;j++){
				scanf("%c", &N[i][j]);				
			}
			scanf("%c", &tmp);
		}
		for (int i = 0;i < y;i++){
			for (int j = 0;j < y;j++){
				scanf("%c", &b);
				n[i][j] = b;
				n90[j][y - i - 1] = b;
				n180[y - i - 1][j] = b;
				n270[y - j - 1][i] = b;
			}
			scanf("%c", &tmp);
		}

		
		int tn = 0, tn90 = 0, tn180 = 0, tn270 = 0;
		for (int i = 0;i < x - y;i++){
			for (int j = 0;j < x - y;j++){
				int w = 0, w90 = 0, w180 = 0, w270 = 0;
				for (int k = 0;k < y;k++){
					for (int l = 0;l < y;l++){
						if(N[i + k][j + l] == n[l][k]) w++;
						if(N[i + k][j + l] == n90[l][k]) w90++;
						if(N[i + k][j + l] == n180[l][k]) w180++;
						if(N[i + k][j + l] == n270[l][k]) w270++;
					}
				}
				if (w == y * y) tn++;
				if (w90 == y * y) tn90++;
				if (w180 == y * y) tn180++;
				if (w270 == y * y) tn270++;
			}
		}
		cout << tn << ' ' << tn90 << ' ' << tn180 << ' ' << tn270 << endl;
	}
}