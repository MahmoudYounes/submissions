#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF 1<<30
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

vector<vi> AdjList;
vi vis;
int m, d;

void bfs(int s)
{
	vis[s] = 0;
	queue<int> q;
	q.push(s);
	while (!q.empty())
	{
		int u = q.front();
		q.pop();
		for (int i = 0; i < AdjList[u].size(); i++)
		{
			int v = AdjList[u][i];
			if (vis[v] == -1)
			{
				q.push(v);
				vis[v] = vis[u] + 1;
			}
		}
	}
}

int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	DRI(n);
	AdjList.resize(n);
	int x, y, t, s;
	for (int i = 0; i < n; i++)
	{
		cin >> x;
		for (int j = 0; j < x; j++)
		{
			cin >> y;
			AdjList[i].push_back(y);
		}
	}
	cin >> t;
	for (int i = 0; i < t; i++)
	{
		m = 0, d = 0;
		cin >> s;
		vis.assign(n + 1, -1);
		bfs(s);
		vi l(n, 0);
		for (int i = 0; i < n; i++)
		{
			if (vis[i]>0)
				l[vis[i]]++;
		}
		int maxn = INT_MIN, max_id = -1;
		for (int i = 1; i < n; i++)
		{
			if (l[i]>0)
			{
				if (maxn < l[i])
				{
					maxn = l[i];
					max_id = i;
				}
			}
		}
		if (max_id == -1) cout << 0 << endl;
		else cout << maxn << ' ' << max_id << endl;
	}
	return 0;
}