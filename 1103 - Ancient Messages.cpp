#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF INT_MAX
#define NINF INT_MIN
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

int AdjMat[500][500];
int arr[500][500];
int x, y;
int dr[] = { 1, 1, 0, -1, -1, -1, 0, 1 };
int dc[] = { 0, 1, 1, 1, 0, -1, -1, -1 };
int holes;
int FFillBG(int r, int c, int c1, int c2)
{
	if (r >= x || r < 0 || c >= y || c < 0)return 0;
	if (AdjMat[r][c] == c2) return 0;
	if (arr[r][c] == 1)return 0;
	arr[r][c] = 1;
	int ans = 1;
	for (int i = 0; i < 8; i++)
		ans += FFillBG(r + dr[i], c + dc[i], 0, 1);
	return ans;
}
int FFill(int r, int c, int c1, int c2)
{
	if (r >= x || r < 0 || c >= y || c < 0)return 0;
	if (AdjMat[r][c] == c1)
	{
		if (arr[r][c] == 0)
		{
			holes++;
			arr[r][c] = 1;
			for (int i = 0; i < 8; i++)
				FFillBG(r + dr[i], c + dc[i], 0, 1);
			return 0;
		}
	}
	if (arr[r][c] == 1)return 0;
	arr[r][c] = 1;
	int ans = 1;
	for (int i = 0; i < 8; i++)
		ans += FFill(r + dr[i], c + dc[i], 0, 1);
	return ans;
}

int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif		
	map<char, string> mymap;
	mymap['0'] = "0000";
	mymap['1'] = "0001";
	mymap['2'] = "0010";
	mymap['3'] = "0011";
	mymap['4'] = "0100";
	mymap['5'] = "0101";
	mymap['6'] = "0110";
	mymap['7'] = "0111";
	mymap['8'] = "1000";
	mymap['9'] = "1001";
	mymap['a'] = "1010";
	mymap['b'] = "1011";
	mymap['c'] = "1100";
	mymap['d'] = "1101";
	mymap['e'] = "1110";
	mymap['f'] = "1111";
	char Out[] = { 'W', 'A', 'K', 'J', 'S', 'D' };
	while (cin >> x >> y)
	{
		if (!x&&!y) break;
		y *= 4;
		memset(AdjMat, 0, sizeof(AdjMat));
		memset(arr, 0, sizeof(arr));
		char in;
		for (int i = 0; i < x; i++)
		{
			for (int j = 0; j < y; j++)
			{
				cin >> in;
				string s = mymap[in];
				for (int k = 0; k < s.size(); k++, j++)
				{
					if (s[k] == '0')
						AdjMat[i][j] = 0;
					else
						AdjMat[i][j] = 1;
				}
				j--;
			}
		}
		
		int bx, by;
		bool found = 0;
		for (int i = 0; i < x&&!found; i++)
		{
			if (AdjMat[0][i] == 0)
			{
				bx = 0; by = i; found = 1;
			}
		}
		for (int i = 0; i < x&&!found; i++)
		{
			if (AdjMat[x - 1][i] == 0)
			{
				bx = x - 1; by = i; found = 1;
			}
		}
		for (int i = 0; i < y&&!found; i++)
		{
			if (AdjMat[i][0] == 0)
			{
				bx = i; by = 0; found = 1;
			}
		}
		for (int i = 0; i < y&&!found; i++)
		{
			if (AdjMat[i][y - 1] == 0)
			{
				bx = i; by = y - 1; found = 1;
			}
		}
		for (int i = 0; i < x; i++)
		{
			for (int j = 0; j < y; j++)
				cout << AdjMat[i][j];
			cout << endl;
		}
		cout << endl;

		FFillBG(bx, by, 0, 1);		
		
		for (int i = 0; i < x; i++)
		{
			for (int j = 0; j < y; j++)
				cout << arr[i][j];
			cout << endl;
		}
		//
		vi hNum;
		for (int i = 0; i < x; i++)
		for (int j = 0; j < y; j++)
		{
			if (AdjMat[i][j] == 1 && arr[i][j] == 0)
			{
				holes = 0;
				FFill(i, j, 0, 1);
				hNum.push_back(holes);
			}
		}
		string sout;
		for (int i = 0; i < hNum.size(); i++)
			sout += Out[hNum[i]];
		sort(ALL(sout));
		cout << sout;
		cout << endl;
	}
	return 0;
}