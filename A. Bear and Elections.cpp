#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF INT_MAX
#define NINF INT_MIN
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

ll _sieve_size;
bitset<10000010> bs; // 10^7 should be enough for most cases
vi primes; // compact list of primes in form of vector<int>
void sieve(ll upperbound)
{ // create list of primes in [0..upperbound]
	_sieve_size = upperbound + 1; // add 1 to include upperbound
	bs.set(); // set all bits to 1
	bs[0] = bs[1] = 0; // except index 0 and 1
	for (ll i = 2; i <= _sieve_size; i++)
	if (bs[i])
	{
		// cross out multiples of i starting from i * i!
		for (ll j = i * i; j <= _sieve_size; j += i) bs[j] = 0;
		primes.push_back((int)i); // also add this vector containing list of primes
	}
} // call this method in main method
bool isPrime(ll N)
{ // a good enough deterministic prime tester
	if (N <= _sieve_size) return bs[N]; // O(1) for small primes
	for (int i = 0; i < (int)primes.size(); i++)
	if (N % primes[i] == 0) return false;
	return true; // it takes longer time if N is a large prime!
} // note: only work for N <= (last prime in vi "primes")^2


int gcd(int a, int b)
{
	return b == 0 ? a : gcd(b, a%b);
}

class UnionFind
{
private:
	vi p;			//vector of integers to store parents of each vertex
	vi r;			//vector of integers to store ranks for each subtree
public:
	UnionFind(int N)
	{
		r.assign(N, 0);		//all disjoint trees have height zero
		p.assign(N, 0);		//just resizing array	
		fl2(i, N) p[i] = i; //each is it's own representitive for now
	}
	int FindSet(int i)
	{
		return p[i] == i ? i : (p[i] = FindSet(p[i])); // the p[i] = FindSet(p[i]) is for the heuristic path compression
		// after iterating over each sub-node in a tree .. assign the parent of this node to be the root directly
		//in order for latter itteration to be in O(1)
	}
	bool isSameSet(int i, int j)
	{
		return FindSet(i) == FindSet(j);
	}
	void UnionSet(int i, int j)
	{
		if (!isSameSet(i, j))
		{
			int x = FindSet(i), y = FindSet(j);
			if (r[x] > r[y]) p[y] = x;
			else
			{
				p[x] = y;
				if (r[x] == r[y])
					r[y]++;
			}
		}
	}
};

class SegmentTree
{
private:
	vi A, st;
	int n;
	int left(int i) { return i << 1; }
	int right(int i) { return (i << 1) + 1; }
	void build(int i, int l, int r)
	{
		if (l == r) st[i] = l;
		else
		{
			build(left(i), l, (l + r) / 2);
			build(right(i), ((l + r) / 2) + 1, r);
			int x = left(i), y = right(i);
			st[i] = A[x] >= A[y] ? y : x;
		}
	}
	int rmq(int ind, int l, int r, int i, int j)
	{
		if (i > r || j < l) return -1;
		if (i >= l&&j <= r)return st[ind];

		int x = rmq(left(ind), l, (l + r) / 2, i, j), y = rmq(right(ind), r, ((l + r) / 2) + 1, i, j);
		if (x == -1) return y;
		if (y == -1) return x;
		return A[x] >= A[y] ? y : x;
	}
public:
	SegmentTree(const vi &_A)
	{
		A = _A; n = _A.size();
		st.assign(4 * n, 0);
		build(1, 0, n - 1);
	}
	int rmq(int i, int j){ return rmq(1, 0, n - 1, i, j); }
};


class FenwickTree
{
private:
	vi ft;
	int n;
	int LSOne(int no) { return (no&(-no)); }
	int rsq(int a)
	{
		int sum = 0;
		for (; a != 0; a -= LSOne(a))
			sum += ft[a];
		return sum;
	}
public:
	FenwickTree(const vi & _A)
	{
		n = _A.size();
		ft.assign(n + 1, 0);
		//O(nlog n ) to build the tree
		for (int i = 0; i < n; i++) //loop on all _A (n)
		for (int k = _A[i]; k < n; k += LSOne(k))	//log (n) 		
			ft[k] += 1;
	}
	void update(int idx, int val)
	{
		for (; idx <= n; idx += LSOne(idx))
			ft[idx] += val;
	}
	int rsq(int a, int b)
	{
		return rsq(b) - (a == 1 ? 0 : rsq(a - 1));
	}
};

//==========================================================================================\\
//others

//==========================================================================================\\


//A
int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	DRI(n);
	priority_queue <int> q;
	int l, m,c=0;
	cin >> l;
	for (int i = 1; i < n; i++) { cin >> m; q.push(m); }
	while (q.top() >= l)
	{
		m = q.top();
		q.pop();
		m--;
		q.push(m);
		l++;
		c++;
		
	}
	cout << c << endl;
	return 0;
}



