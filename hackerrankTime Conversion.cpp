#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;


int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	int h, m, s;
	string hh, mm, ss, st;
	getline(cin, st);
	hh.push_back(st[0]);
	hh.push_back(st[1]);
	mm.push_back(st[3]);
	mm.push_back(st[4]);
	ss.push_back(st[6]);
	ss.push_back(st[7]);
	if (st[8] == 'A')
	{
		if (hh == "12")
			hh = "00";
		cout << hh << ':' << mm << ':' << ss << endl;
	}
	else
	{
		h = stoi(hh);
		if (h!=12)
		h += 12;
		cout << h << ':' << mm << ':' << ss << endl;
	}
	return 0;
}
