#include<iostream>
#include<vector>
#include<map>
#include<algorithm>
#include<string>
#include<queue>
using namespace std;
int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int m,n;
	int t = 1;
	while (cin >> n)
	{
		map<string, int> order_map;
		map<int, string>order_map_rev;
		vector<pair<int, vector<int>>> in_deg(n);
		string s, ss;
		for (int i = 0; i < n; i++)
		{
			cin >> s;			
			order_map[s] = i;
			in_deg[i].first = 0;
			order_map_rev[i] = s;
		}
		cin >> m;
		for (int i = 0; i < m; i++)
		{
			cin >> s >> ss;
			int id1 = order_map[s],id2=order_map[ss];
			in_deg[id1].second.push_back(id2);
			in_deg[id2].first++;
		}
		priority_queue<int> q;
		for (int i = 0; i < in_deg.size(); i++)
		{
			if (in_deg[i].first == 0)
				q.push(-i);
		}
		vector<int> ls;
		while (!q.empty())
		{
			int u = -1*q.top();
			q.pop();
			ls.push_back(u);
			int size = in_deg[u].second.size();
			for (int i = 0; i < size; i++)
			{
				int v = in_deg[u].second[i];
				in_deg[v].first--;
				if (in_deg[v].first == 0)
					q.push(-1*v);
			}
		}
		cout << "Case #" << t++ << ": Dilbert should drink beverages in this order: ";
		for (int i = 0; i < ls.size(); i++)
		{
			cout << order_map_rev[ls[i]];
			if (i < ls.size() - 1) cout << ' ';			
			else cout << '.';
		}		
		cout << endl<<endl;
	}
}