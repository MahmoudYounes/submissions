#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt","r",stdin);
	#endif

	string in,sub1,sub2,rsub1,rsub2;
	int n1,n2,rn1;
	cin>>in;
	
	sub1=in.substr(0,2);
	rsub1=sub1;
	swap(rsub1[0],rsub1[1]);

	sub2=in.substr(3,2);	
	rsub2=sub2;
	swap(rsub2[0],rsub2[1]);
	
	n1=atoi(sub1.c_str());
	n2=atoi(sub2.c_str());
	rn1=atoi(rsub1.c_str());	
	if (n2<rn1&&rn1<=59)
		cout<<sub1<<':'<<rsub1<<endl;
	else
	{
		n1++;
		if ((n1>=6&&n1<=9))
		{
			cout<<"10:01"<<endl;			
		}
		else if((n1>=16&&n1<=19))
		{
			cout<<"20:02"<<endl;
		}
		else if (n1>=23&&n2>=32)
		{
			cout<<"00:00"<<endl;
		}
		else
		{
			if (n1<6)
			{
				cout<<'0'<<n1<<':'<<n1<<'0'<<endl;
			}
			else
			{
				string out,outr;
				stringstream ss;
				ss<<n1;
				ss>>out;
				outr=out;
				swap(outr[0],outr[1]);
				cout<<out<<':'<<outr<<endl;
			}
		}
	}

	return 0;
}