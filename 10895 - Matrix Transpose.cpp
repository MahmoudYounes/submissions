#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF INT_MAX
#define NINF INT_MIN
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	int n, m;
	while (cin>>n>>m)
	{
		vector<vi> arr(n, vector<int>(m));
		fl2(i, n)
		{
			arr[i].assign(m, 0);
			DRI(x);
			vi pos(x);
			fl2(j, x)
			{
				cin >> pos[j];
				pos[j]--;
			}
			fl2(j, x)
			{
				DRI(y);
				arr[i][pos[j]] = y;
			}
		}
		vector<vi> arrt(m, vector<int>(n));
		fl2(i, n)
			fl2(j, m)
			arrt[j][i] = arr[i][j];
		cout << m << ' ' << n << endl;
		fl2(i, m)
		{
			vi pos;
			fl2(j, n)
			{
				if (arrt[i][j] != 0)
					pos.push_back(j);
			}
			cout << pos.size();
			fl2(j, pos.size())
				cout << ' '<<pos[j] + 1;
			cout << endl;
			fl2(j, n)
			{
				if (arrt[i][j]) cout << arrt[i][j];
				if (arrt[i][j]&&j < n - 1) cout << ' ';
			}
			cout << endl;
		}
	}
			
	return 0;
}