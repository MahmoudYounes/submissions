#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF INT_MAX
#define NINF INT_MIN
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

int AdjMat[150][150];
bool arr[150][150];
int n, m;

int dr[] = { 1, 1, 0, -1, -1, -1, 0, 1 };
int dc[] = { 0, 1, 1, 1, 0, -1, -1, -1 };
int FFill(int r, int c, int c1, int c2)
{
	if (r >= n || r < 0 || c >= m || c < 0) return 0;
	if (AdjMat[r][c] == c2)return 0;
	if (arr[r][c])return 0;
	arr[r][c] = 1;
	int ans = 1;
	for (int i = 0; i < 8; i++)
	{
		ans += FFill(r + dr[i], c + dc[i], 0, 1);
	}
	return ans;

}
int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	DRI(t);
	string s;
	getline(cin, s);
	getline(cin, s);
	while (t--)
	{		
		n = 0; 
		m = 0;
		while (getline(cin,s) && (s[0]=='L'||s[0]=='W'))
		{
			m = s.size();			
			for (int i = 0; i < m; i++)
			{
				if (s[i] == 'L')AdjMat[n][i] = 1;
				if (s[i] == 'W')AdjMat[n][i] = 0;
			}
			n++;			
		}			
		int x, y;
		while (s.size()>0)
		{
			stringstream ss;
			memset(arr, 0, sizeof(arr));
			ss << s;
			ss >> x;
			ss >> y;
			//do floodfill
			cout << FFill(x-1, y-1, 0, 1)<<endl;
			getline(cin, s);
		}
		if (t > 0)
			cout << endl;
	}
	return 0;
}