#include <iostream>
#include <map>
#include <vector>
#include <limits.h>
#include <algorithm>
#include <fstream>
#include <string>
#include <string.h>
using namespace std;

string no;
int m,s;
long long dp[110][910];
long long sol(int ci,int cs,long long & min_no, long long & max_no)
{       
    if (ci>=m)  
    {
        if(cs==s)
        {               

            return atoll(no.c_str());
        }
        else return -1;
    }
    if (dp[ci][cs]!=-1) return dp[ci][cs];  
    int i='0';
    if(ci==0) i='1';    
    for (;i<='9';i++)
    {       
        no.push_back(i);
        long long xxx=sol(ci+1,cs+int(i-'0'),min_no,max_no);
        if(xxx!=-1&&xxx!=0)
        {           
            if(min_no>xxx)min_no=xxx;       
            if(max_no<xxx)max_no=xxx;                   
        }               
        no.erase(no.size()-1,1);
        dp[ci][cs]=xxx; 
    }   
    
}


int main()
{
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);    
    #endif
    
    memset(dp,-1,sizeof(dp));
    cin>>m>>s;  
    long long min_no=LLONG_MAX,max_no=LLONG_MIN;        
    no="";
    sol(0,0,min_no,max_no);
    if (max_no==LLONG_MIN&&min_no==LLONG_MAX) cout<<-1<<' '<<-1<<endl;
    else cout<<min_no<<' '<<max_no<<endl;
}