#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>

using namespace std;

#define RI(x) scanf("%d", &(x))
#define DRI(x) int (x); RI((x))
#define DII(x, y) RI((x)); RI((y))
#define DRII(x, y) DRI((x)); DRI((y))

#define vi(vec) vector<int> (vec)
#define vpii(vec) vector<pair<int, int> > (vec)

#define lop(i, s, e) for(int (i) = (s);(i) < (e);(i)++)
#define lopi(i, s, e, inc) for(int (i) = (s);(i) < (e);(i) += (inc))

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	DRII(n, p);
	string s;
	cin >> s;
	p--;
	if (p >= n / 2)
	{
		reverse(s.begin(), s.end());
		p = n - p - 1;
	}

	vector<int> idx;
	int sm = n / 2, steps = 0;
	lop(i, 0, n / 2)
	{		
		if (s[i] != s[n - i - 1])
		{
			int tx = abs(s[i] - s[n - i - 1]);
			steps += min(tx, 26 - tx );
			idx.push_back(i);
		}
	}
	if(idx.size() == 0)
	{
		cout << 0 << endl;
		return 0;
	}

	int dis = idx.back() - idx[0], cd = dis;
	if (p <= idx[0])
	{
		cd += idx[0] - p;
	}
	else if(p >= idx.back())
	{
		cd += p - idx.back();
	}
	else
	{
		cd += min(idx.back() - p, p - idx[0]);
	}
	steps += cd;
	cout << steps << endl;
	return 0;
}