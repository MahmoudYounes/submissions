#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#define RI(x) scanf("%d", &(x))
#define DRI(x) int (x); RI((x))
#define DII(x, y) RI((x)); RI((y))
#define DRII(x, y) DRI((x)); DRI((y))

#define vi(vec) vector<int> (vec)
#define vpii(vec) vector<pair<int, int> > (vec)

#define lop(i, s, e) for(int (i) = (s);(i) < (e);(i)++)
#define lopi(i, s, e, inc) for(int (i) = (s);(i) < (e);(i) += (inc))

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	DRII(r, c);
	DRI(k);

	int cn = r * c,tcc = cn / k, ctcc = tcc, ck = k;
	if (k == 1) tcc = 0;
	printf("%d ", (ck == 1? (cn - (k - 1) * tcc):tcc));
	ctcc = (ck == 1? (cn - (k - 1) * tcc):tcc);
	lop(i, 1, r + 1)
	{
		if (i % 2)
		{
			lop(j, 1, c + 1)
			{
				if(ctcc == 0)
				{
					ck--;
					printf("\n%d ", (ck == 1? (cn - (k - 1) * tcc):tcc));
					ctcc = (ck == 1? (cn - (k - 1) * tcc):tcc);
				}
				printf("%d %d ", i, j);
				ctcc--;
			}
		}
		else
		{
			for (int j = c;j > 0;j--)
			{
				if(ctcc == 0)
				{
					ck--;
					printf("\n%d ", (ck == 1? (cn - (k - 1) * tcc):tcc));
					ctcc = (ck == 1? (cn - (k - 1) * tcc):tcc);
				}
				printf("%d %d ", i, j);	
				ctcc--;
			}
		}
	}
	cout << endl;
	return 0;
}