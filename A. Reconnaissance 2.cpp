#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <fstream>
#include <map>
#include <math.h>
#include <cmath>
#include <limits.h>
using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE	
		freopen("in.txt","r",stdin);
	#endif

	int n;
	cin>>n;
	vector<int> vec(n);
	for (int i=0;i<n;i++)
	{
		cin>>vec[i];
	}
	int dif=INT_MAX,i1=0,i2=0;
	for (int i=0;i<n-1;i++)
	{		
		if (abs(vec[i]-vec[i+1])<dif)
		{
			dif=abs(vec[i]-vec[i+1]);
			i1=i;
			i2=i+1;
		}
	}
	if (abs(vec[n-1]-vec[0])<dif) i1=n-1,i2=0;
	cout<<i1+1<<' '<<i2+1<<endl;
	return 0;
}