#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>

using namespace std;

#define RI(x) scanf("%d", &(x))
#define DRI(x) int (x); RI((x))
#define DII(x, y) RI((x)); RI((y))
#define DRII(x, y) DRI((x)); DRI((y))

#define vi(vec) vector<int> (vec)
#define vpii(vec) vector<pair<int, int> > (vec)

#define lop(i, s, e) for(int (i) = (s);(i) < (e);(i)++)
#define lopi(i, s, e, inc) for(int (i) = (s);(i) < (e);(i) += (inc))

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	string s, s1, ans;
	cin >> s;
	bool t1 = 0;
	for (int i = 0;i < s.length();)
	{
		int j = i;
		s1 += s[i];
		while(j < s.length() && s[j] == s[j+1]) j++;
		if (j - i >= 1) s1 += s[i];
		i = j + 1;
	}
	ans += s1[0];
	if(s1.length() > 1)
		ans += s1[1];
	if(s1.length() > 2)
		ans += s1[2];
	for (int i = 3;i < s1.length();i++)
	{
		int ln = ans.length();
		if(ans[ln - 3] == ans[ln - 2] && s1[i] == ans[ln - 1])
			continue;
		else
			ans += s1[i];
	}
	cout << ans << endl;
	return 0;
}