#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;
int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	int n, m, arr[1000000];
	int sums[1000001];
	memset(sums, 0, sizeof(sums));

	cin >> n >> m;
	for (int i = 0;i < n;i++)
	{
		cin >> arr[i];
		if (i == 0)
		{
			sums[i+1] = arr[i];
		}
		else
			sums[i+1] = arr[i] + sums[i];
	}

	for (int i = 1;i < n - 1;i++)
	{
		for (int j = i+1; j < n;j++)
		{
			if (!((sums[j] - sums[i - 1]) % m))
			{
				cout << "YES" << endl;
				return 0;
			}
		}
	}
	cout << "NO" << endl;
}