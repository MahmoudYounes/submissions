#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF 1<<30
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

int AdjMat[1010][1010];
int dx[] = { 0, 0, 1, -1 };
int dy[] = { 1, -1, 0, 0 };
int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	DRI(t);
	while (t--)
	{
		DRII(n, m);
		int x;
		vector<vii>AdjList(n*m);
		fl2(i, n)
			fl2(j, m)
			cin >> AdjMat[i][j];

		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
			{				
				for (int k = 0; k < 4; k++)
				{
					int r1 = i, r2 = j;
					if (r1 + dx[k] >= 0 && r1 + dx[k] < n && r2 + dy[k] >= 0 && r2 + dy[k] < m)
					{
						int cno = i*m + j;
						r1 = i + dx[k], r2 = j + dy[k];
						//if (r1*m + r2 >= m*n || r1*m + r2 < 0) continue;
						AdjList[cno].push_back(ii(AdjMat[i][j], r1*m + r2));
					}
				}
			}
		}

		vi dis(n*m, INF);
		dis[0] = 0;
		priority_queue <ii, vii, greater<ii>> pq;
		pq.push(ii(0, 0));
		while (!pq.empty())
		{
			ii u = pq.top();
			pq.pop();
			if (u.second == n*m - 1)break;
			for (int i = 0; i < AdjList[u.second].size(); i++)
			{
				ii v = AdjList[u.second][i];
				if (v.first>dis[v.second]) continue;
				if (dis[v.second] > dis[u.second] + v.first)
				{
					dis[v.second] = dis[u.second] + v.first;
					pq.push(ii(dis[v.second], v.second));
				}
			}
		}
		cout << dis[(n*m) - 1] + AdjMat[n - 1][m - 1] << endl;
		AdjList.clear();
		dis.clear();
		memset(AdjMat, 0, sizeof(AdjMat));
	}
	return 0;
}