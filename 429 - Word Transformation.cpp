#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF 1<<30
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\


vector<vi> AdjList;
vi vis;
void bfs(int s)
{
	vis[s] = 0;
	queue<int> q;
	q.push(s);
	while (!q.empty())
	{
		int u = q.front();
		q.pop();
		for (int i = 0; i < AdjList[u].size(); i++)
		{
			int v = AdjList[u][i];
			if (vis[v] == -1)
			{
				q.push(v);
				vis[v] = vis[u] + 1;
			}
		}
	}

}

bool compare(string s, string t)
{
	int d = 0;
	for (int i = 0; i < min(s.size(),t.size()); i++)
	{		
		if (s[i] != t[i])d++;
	}
	if (d==1)return true;
	return false;
}
int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	string line;
	stringstream st;
	map<string, int>cypher;
	map<int, string>decypher;
	DRI(t);
	int c = t;
	getline(cin, line);
	getline(cin, line);
	while (t--)
	{
		if (t < c - 1) cout << endl;
		int n = 0;
		vector<string> dic;
		while (getline(cin, line) && line != "*")
		{
			cypher[line] = n;
			decypher[n++] = line;
			dic.push_back(line);
		}
		AdjList.resize(n);
		vis.assign(n, 0);
		for (int i = 0; i < dic.size(); i++)
		{
			for (int j = 0; j < dic.size(); j++)
			{
				if (compare(dic[i], dic[j]))
				{
					AdjList[cypher[dic[i]]].push_back(cypher[dic[j]]);
					AdjList[cypher[dic[j]]].push_back(cypher[dic[i]]);
				}
			}
		}
		string w1, w2;
		while (getline(cin, line)&& line.size()>0)
		{
			st << line;
			st >> w1;
			st >> w2;
			int nw1 = cypher[w1];
			int nw2 = cypher[w2];
			vis.assign(n, -1);
			bfs(nw1);
			cout << w1 << ' ' << w2 << ' ' << vis[nw2] << endl;
			st.clear();
			vis.clear();
		}	
		AdjList.clear();
		cypher.clear();
		decypher.clear();
	}
	
	return 0;
}