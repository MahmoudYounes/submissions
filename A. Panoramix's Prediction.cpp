#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

bool primes[100];
void prep()
{
	memset(primes, 1, sizeof(primes));
	primes[1] = 0;
	for (int i = 2;i < 300; i++)
	{
		if(primes[i] == 1)
		{
			for (int j = i * i;j < 300;j = j + i)
			{
				primes[j] = 0;
			}
		}
	}
}

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif
	int n,m;
	cin >> n >> m;
	prep();
	bool cond = 0;
	bool cond2 = 0;
	for (int i = n + 1; i < 60;i++)
	{	
		if(primes[i] && i == m) 
			cond = 1;
		else if (primes[i]) break;
	}

	cout << (cond? "YES": "NO") << endl;
	return 0;
}