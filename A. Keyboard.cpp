#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF 1<<30
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\




int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	char x, xc;
	string arr[] = { "qwertyuiop", "asdfghjkl;", "zxcvbnm,./" }, sti, sto;
	cin >> x;
	if (x == 'R') xc = -1;
	else xc = 1;
	getline(cin, sti);
	getline(cin, sti);
	for (int i = 0; i < sti.size(); i++)
	{
		for (int j = 0; j < 3; j++)
		{		
			for (int k=0; k < arr[j].size(); k++)
			{
				if (sti[i] == arr[j][k]) sto.push_back(arr[j][k + xc]);
			}		
		}
	}
	cout << sto << endl;
	return 0;
}

/*
vector<vector<int>> AdjList;
vector<int> dist;
void bfs(int u)
{
dist[u] = 0;
queue<int> q;
q.push(u);
while (!q.empty())
{
int v = q.front();
q.pop();
for (int i = 0; i<AdjList[v].size(); i++)
{
int n = AdjList[v][i];
if (dist[n] == INF)
{
q.push(n);
dist[n] = dist[v] + 1;
}
}
}

}

int main() {
/* Enter your code here. Read input from STDIN. Print output to STDOUT 
freopen("in.txt", "r", stdin);
int t;
cin >> t;
while (t--)
{
	int n, m, x, y, s;
	cin >> n >> m;
	AdjList.resize(n + 2);
	dist.assign(n + 2, INF);
	for (int i = 0; i<m; i++)
	{
		cin >> x >> y;
		AdjList[x].push_back(y);
		AdjList[y].push_back(x);
	}
	cin >> s;
	bfs(s);
	for (int i = 1; i <= n; i++)
	{
		if (i == s) continue;
		if (dist[i] == INF) cout << -1 << ' ';
		else cout << dist[i] * 6 << ' ';
	}
	cout << endl;
	AdjList.clear();
	dist.clear();
}
return 0;
}

*/