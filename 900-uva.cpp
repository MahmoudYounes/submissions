#include <iostream>
using namespace std;

int main(){
	long long fibs[55], sums[55];
	fibs[0] = 0;
	fibs[1] = 1;
	fibs[2] = 1;
	for (int i = 3;i < 53;i++){
		fibs[i] = fibs[i - 1] + fibs[i - 2];
	}

	sums[0] = 0;
	sums[1] = 1;
	sums[2] = 2;
	sums[3] = 3;
	for (int i = 4;i < 53;i++){
		sums[i] = sums[i - 1] + fibs[i - 1];
	}

	int x;
	while (cin >> x){
		if(x == 0) break;
		cout << sums[x] <<endl;
	}
	return 0;
}