#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <map>
using namespace std;

int main()
{
    #ifndef ONLINE_JUDGE
        freopen("in.txt", "r", stdin);
    #endif
    int acc[150];
    memset(acc, 0, sizeof(acc));

    int n, x;
    cin >> n >> x;
    acc[0] = x;
    for (int i = 1;i < n;i++)
    {
        cin >> x;
        acc[i] = acc[i - 1] + x;
    }
    int count = 0;
    vector<pair<int,int>> ans;
    int l = 0, r = 0;
    while(l < n && r < n)
    {
        if(acc[r] - acc[l] != 0)
        {
            count++;
            ans.push_back(make_pair(l, r));
            l = r + 1;

        }
        r++;
    }
    if(ans.size() != 0)
    {
        cout << "YES\n";
        cout << ans.size() << endl;
        for (int i = 0;i < ans.size();i++)
        {
            cout << ans[i].first + 1 << ' ' << ans[i].second + 1 << endl;
        }
    }
    else
        cout << "NO\n";
    

}