#include<iostream>
#include<vector>
#include<stdio.h>
#include<string.h>
using namespace std;

char AdjMat[110][110];
int dr[] = { 1, 0, -1, 0 };
int dc[] = { 0, 1, 0, -1 };
int main()
{	
	int n, m, s;	
	while (cin >> n >> m >> s)
	{
		if (n == 0)break;		
		memset(AdjMat, '.', sizeof(AdjMat));
		char x;
		int rr, rc;
		int k;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
			{
				cin >> x;
				if (x == 'N')
				{
					rr = i; rc = j;
					k = 2;
				}
				if (x == 'S')
				{
					rr = i; rc = j;
					k = 0;
				}
				if (x == 'L')
				{
					rr = i; rc = j;
					k = 1;
				}
				if (x == 'O')
				{
					rr = i; rc = j;
					k = 3;
				}
				AdjMat[i][j] = x;
			}
		}
		int kr = dr[k], kc = dc[k];
		int counter = 0;
		bool stall = 0;
		for (int i = 0; i < s; i++)
		{
			cin >> x;
			if (x == 'E')
			{
				k++;
				if (k >= 4)k -= 4;
				kr = dr[k];
				kc = dc[k];
			}
			else if (x == 'D')
			{
				k--;
				if (k < 0)k += 4;
				kr = dr[k];
				kc = dc[k];
			}
			else
			{
				rr += kr;
				rc += kc;
				if ((rr >= n || rc >= m) || (rr < 0 || rc < 0) || (AdjMat[rr][rc] == '#'))
				{
					rr -= kr;
					rc -= kc;
				}
				if (AdjMat[rr][rc] == '*')
				{
					counter++;
					AdjMat[rr][rc] = '.';
				}
			}
		}
		cout << counter<<endl;		
	}
}