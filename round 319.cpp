#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF 1<<30
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers



//==========================================================================================\\
//others

//==========================================================================================\\


/* A
int main()
{
#ifndef ONLINE_JUDGE
freopen("in.txt", "r", stdin);
#endif

DRII(n, m);
int count = 0;
for (int i = 1; i*i <= m; i++)
{
if (m%i == 0 && m / i <= n&&i <= n)
{
if (i != m / i)count += 2;
else count++;
}
}
cout << count << endl;
return 0;
}
*/

/*B*/

int n, m;
bool dp[1000][1000000];
bool vis[1000][1000000];
int arr[1000000];
bool sol(int sum, int idx)
{
	if (idx >= n)
	{
		if (sum != 0 && sum%m == 0)return 1;
		else return 0;
	}
	if (sum != 0 && sum%m == 0)return 1;
	if (vis[sum][idx]== true)
		return dp[sum][idx];
	bool cond = 0;
	cond = cond || sol(sum, idx + 1);
	cond = cond || sol((sum%m + arr[idx] % m), idx + 1);
	dp[sum][idx] = cond;
	vis[sum][idx] = 1;

}
int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	cin >> n >> m;
	memset(dp, 0, sizeof(dp));
	memset(vis, 0, sizeof(vis));	
	for (int i = 0; i < n; i++)cin >> arr[i];
	if (sol(0, 0))
		cout << "YES" << endl;
	else cout << "NO" << endl;

	return 0;
}