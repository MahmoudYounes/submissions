#include <iostream>
using namespace std;
int main()
{
	int n;
	cin>>n;
	if(n%2==1)
	{
		cout<<-1<<endl;
	}
	else 
	{
		int e=2,o=1;
		for (int i=1;i<=n;i++)
		{
			if(i%2==1){ cout<<e<<' ';e+=2;}
			else { cout<<o<<' ';o+=2;}
		}
		cout<<endl;
	}
	return 0;
}