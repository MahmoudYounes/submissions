#include <iostream>
#include <string>
#include <algorithm>
#include <limits>
#include <string.h>
using namespace std;

int x,y;
string cMinN, cMaxN, fmx, fmn;

int dpMi[101][1010],dpMa[1010];
void recMin(int cMinS)
{
	if(cMinN.length() > x || cMinS > y)
	{
		return;
	}

	if (dpMi[cMinN.length()][cMinS] != -1)
		return;
	if (cMinS == y && cMinN.length() == x) 
	{	
		dpMi[cMinN.length()][cMinS] == 1;
		fmn = cMinN;
	}
	

	for (int i = 0 ;i < 10;i++)
	{
		if(cMinN.length() == 0 && i == 0)
			continue;
		cMinN.push_back(i + '0');
		recMin(cMinS + i);
		cMinN.erase(cMinN.length()-1);
	}
	dpMi[cMinN.length()][cMinS] = 0;
}

void recMax(int cMaxS)
{
	if(cMaxN.length() > x || cMaxS > y)
	{
		return;
	}

	if (dpMa[cMaxS] != -1)
		return;
	if (cMaxS == y && cMaxN.length() == x) 
	{	
		dpMa[cMaxS] == 1;
		fmx = cMaxN;
	}

	
	for (int i = 9;i >= 0;i--)
	{
		if(cMaxN.length() == 0 && i == 0)
			continue;
		cMaxN.push_back(i + '0');
		recMax(cMaxS + i);
		cMaxN.erase(cMaxN.length()-1);
	}
	dpMa[cMaxS]= 0;
}

int main()
{
	cin >> x >> y;
	if(y == 0 && x != 1)
	{
		cout << "-1 -1";
		return 0;
	}
	else if(y == 0)
	{
		cout << "0 0";
		return 0;
	}
	cMinN = "";
	cMaxN = "";
	memset(dpMa, -1, sizeof(dpMa));
	memset(dpMi, -1, sizeof(dpMi));
	recMin(0); 
	recMax(0);

	if(fmx.length() == 0 && fmn.length() == 0)
		cout << "-1 -1\n";
	else
		cout << fmn << ' ' << fmx << endl;

}



/*
 easier solution: (i want to understand this -_-)
 1- rer => repeat range with 1 inc.
 if(m == 1) {
		if(0 <= s && s <= 9) {
			printf("%d %d\n", s, s);
		}else {
			puts("-1 -1");
		}
		return 0;
	}
	if(s < 1 || m * 9 < s) {
		puts("-1 -1");
		return 0;
	}
	string ans1, ans2;
	int sum1 = 0, sum2 = 0;
	rep(i, m) {
		rer(d, i == 0 ? 1 : 0, 9) {
			if(sum1 + d + 0 <= s && s <= sum1 + d + 9 * (m-i-1)) {
				sum1 += d;
				ans1 += char('0' + d);
				break;
			}
		}
	}
	rep(i, m) {
		for(int d = 9; d >= 0; -- d) {
			if(sum2 + d + 0 <= s && s <= sum2 + d + 9 * (m-i-1)) {
				sum2 += d;
				ans2 += char('0' + d);
				break;
			}
		}
	}

*/