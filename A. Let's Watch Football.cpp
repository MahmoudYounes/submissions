#include <stdio.h>
#include <cstdio>
using namespace std;

#define cin(x) scanf("%d",&x)
#define cout(x) printf("%d",x);
#define endl  printf("\n");

int main()
{
	#ifndef ONELINE_JUDGE
	freopen("in.txt","r",stdin);
	#endif
	int n,m,v;
	scanf("%d %d %d",&n,&m,&v);
	int ans=0;	
	for (int d=0;d<n;d++)
	{
		d++;
		ans++;
	}
	ans*=v;
	ans--;
	printf("%d\n",ans);
}