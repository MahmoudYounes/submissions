#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<functional>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include<bitset>
#include<ctype.h>
#include<string>
#include<map>
#include<set>
#include<queue>
#include<algorithm>
#include<vector>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<stack>
#include<bitset>
#include<fstream>
using namespace std;
// }}}
// #defines {{{
#define _CRT_SECURE_NO_WARNINGS
#define ALL(x) (x).begin(),(x).end()
#define ALL2(arr,x) (arr),(arr+x)
#define fl2(i,n) for(int i=0;i<(n);i++)
#define fl3(i,a,b) for(int i=(a);i<=(b);i++)
#define flx(i,a,x) for(int i=0;i<(a);i+=(x))
#define ifl2(i,n) for(int i=(n);i>=0;i--)
#define ifl3(i,a,b) for(int i=(a);i>=(b);i--)
#define iflx(i,a,x) for(int i=(a)-1;i>=0;i-=(x))
#define RI(x) scanf("%d",&x)
#define DRI(x) int x;RI(x)
#define InDAL(arr,x) int *arr=new int[x]
#define ChDAL(x) new char[x]
#define RII(x,y) scanf("%d %d",&x,&y)
#define DRII(x,y) int x,y;RII(x,y)
#define RIII(x,y,z) scanf("%d %d %d",&x,&y,&z)
#define DRIII(x,y,z) int x,y,z;RIII(x,y,z)
#define PI(x) cout<<x
#define FLch(i,x) for(int i=0;x[i];i++)
#define F first
#define S second
typedef long long ll;
typedef vector<long long int> vi;
typedef pair<long long int, long long int> ii;
typedef vector<ii> vii;
typedef set<int> si;
typedef map<string, int> msi;
#define INF LLONG_MAX
#define NINF -1*INF
#define inF(s) freopen(s,"r",stdin)
#define outF(s) freopen(s,"w",stdout)
//#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
//#define MEMSET_INF 127 // about 2B
//#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//==========================================================================================\\
//others

//==========================================================================================\\

vector<vii>AdjList;
vi dist;
vi p;
void Dijkstra(int s)
{
	dist[s] = 0;
	priority_queue<ii, vii, greater<ii>>pq;
	pq.push(ii(0, s));
	while (!pq.empty())
	{
		ii u = pq.top();
		pq.pop();
		if (u.first > dist[u.second])continue;
		for (int i = 0; i < AdjList[u.second].size(); i++)
		{
			ii v = AdjList[u.second][i];
			if (dist[v.second] > dist[u.second] + v.first)
			{
				dist[v.second] = dist[u.second] + v.first;
				p[v.second] = u.second;
				pq.push(ii(dist[v.second], v.second));
			}
		}
	}
}

void print_path(int u)
{
	if (u == 1) {
		cout << 1<<' '; return;
	}	
	print_path(p[u]);
	cout << u << ' ';
}


int main()
{
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif	
	DRII(n, m);
	int x, y, w;
	AdjList.resize(n + 10);
	dist.assign(n + 10, INF);
	p.assign(n + 10, 0);
	for (int i = 0; i < m; i++)
	{
		cin >> x >> y >> w;
		AdjList[x].push_back(ii(w, y));
		AdjList[y].push_back(ii(w, x));
	}
	Dijkstra(1);
	if (dist[n] == INF) cout << -1 << endl;
	else print_path(n);
	
	return 0;
}