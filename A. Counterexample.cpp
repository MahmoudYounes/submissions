#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
long long gcd(long long a,long long b)
{
    return b==0?a:gcd(b,a%b);
}
int main()
{
    long long l,r;
    cin>>l>>r;
    long long no[3],cond=0;

    for (long long i=l;i<=r;i++)
    {
        for (long long j=i+1;j<=r;j++)
        {
            for (long long k=j+1;k<=r;k++)
            {
                
                if (gcd(i,j)==1&&gcd(j,k)==1&&gcd(i,k)!=1)
                {
                    no[0]=i;
                    no[1]=j;
                    no[2]=k;
                    cond++;
                    break;
                }
            }
            if(cond>0) break;
        }
        if (cond>0)break;
    }
    if(cond>0) 
    {
        cout<<no[0]<<' '<<no[1]<<' '<<no[2]<<endl;
    }
    else cout<<-1<<endl;

}