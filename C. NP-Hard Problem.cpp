#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <string.h>
using namespace std;

#define DRI(x) int (x); scanf("%d", &(x))
#define RI(x) scanf("%d", &(x))
#define REP(i,n) for (int (i) = 0;(i) < (n);(i)++)
#define pii(p) pair<int, int> (p)
#define vpii(p) vector<pair<int, int> > (p);
#define vpiin(p,n) vector<pair<int, int> > (p)(n);
#define vi(p, n)vector<int> (p);
#define vin(p, n)vector<int> (p)(n);

int main()
{
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif

	DRI(n);
	DRI(m);
	int sets[1000000];
	memset(sets, 0, sizeof(sets));
	
	vector<int> A;
	vector<int> B;
	int a,b;
	bool can = 1;
	for (int i = 0;i < m;i++)
	{
		cin >> a >> b;
		if(sets[a] == 0 && sets[b] == 0)
		{
			cout << "new " << a << ' ' << b << endl;
			sets[a] = 1;
			sets[b] = 2;
			A.push_back(a);
			B.push_back(b);
		}
		else if (sets[a] > 0 && sets[b] == 0)
		{
			cout << "a exists in " << sets[a] << endl;
			sets[b] = 3 - sets[a];
			if(sets[b] == 1)
			{
				A.push_back(b);
			}
			else if(sets[b] == 2)
			{
				B.push_back(b);
			}
		}
		else if (sets[a] == 0 && sets[b] > 0)
		{
			cout << "b exists in " << sets[b] << endl;
			sets[a] = 3 - sets[b];
			if(sets[a] == 1)
			{
				A.push_back(a);
			}
			else if(sets[a] == 2)
			{
				B.push_back(a);
			}	
		}
		else
		{
			if(sets[a] == sets[b])
			{
				cout << "a and b exists in same set " << sets[a] << endl;
				can = 0;
			}
		}
	}	

	if(can)
	{
		cout << A.size() << endl;
		for (int i = 0;i < A.size();i++)
		{
			cout << A[i] << ' ';
		}
		cout << endl;
		cout << B.size() << endl;
		for (int i = 0;i < B.size();i++)
		{
			cout << B[i] << ' ';
		}		
		cout << endl;
	}
	else
		cout << -1 << endl;

	return 0;
}